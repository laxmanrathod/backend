package com.mindtree.bookstore.user.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindtree.bookstore.entity.User;
import com.mindtree.bookstore.user.repository.UserRepository;


@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User getUser(int userId) {
		return Optional.ofNullable(userRepository.findById(userId)).get().orElse(null);
	}

}
