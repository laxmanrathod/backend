package com.mindtree.bookstore.user.service;

import com.mindtree.bookstore.entity.User;

public interface UserService {

	/**
	 * returns User object for particular user id
	 * @param userId
	 * @return user
	 */
	public User getUser(int userId);

}
