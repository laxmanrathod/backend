package com.mindtree.bookstore.author.service;

import com.mindtree.bookstore.entity.Author;

public interface AuthorService {

	/**
	 * This method return Author object corresponding to the ID passed
	 * @param authorId
	 * @return Author
	 */
	public Author getAuthor(int authorId);
}
