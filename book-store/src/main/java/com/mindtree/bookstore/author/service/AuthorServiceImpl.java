package com.mindtree.bookstore.author.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mindtree.bookstore.author.repository.AuthorRepository;
import com.mindtree.bookstore.entity.Author;

@Service
public class AuthorServiceImpl implements AuthorService {

	@Autowired
	private AuthorRepository authorRepository;

	@Override
	public Author getAuthor(int authorId) {
		return Optional.ofNullable(authorRepository.findById(authorId)).get().orElse(null);
	}

}
