package com.mindtree.bookstore.cart.service;

import java.util.List;

import com.mindtree.bookstore.cart.dto.CartDto;
import com.mindtree.bookstore.entity.Book;
import com.mindtree.bookstore.entity.Cart;
import com.mindtree.bookstore.entity.User;

public interface CartService {

	/**
	 * adds item to cart based on user
	 * @param user
	 * @param cartDto
	 * @param book
	 */
	public void addCartItem(User user, CartDto cartDto, Book book);

	/**
	 * returns List of Active Cart items for particular user
	 * @param user
	 * @return list of Active Cart items
	 */
	public List<Cart> getCartItems(User user);

}
