package com.mindtree.bookstore.cart.dto;

import com.mindtree.bookstore.entity.Cart;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CartItemResource {

	private Integer cartId;
	private String format;
	private String title;
	private String name;
	private Integer quantity;
	private Double price;
	private Integer bookId;

	public CartItemResource(Cart cart) {
		this.cartId = cart.getCartId();
		this.format = cart.getBook().getFormat();
		this.title = cart.getBook().getTitle();
		this.name = cart.getBook().getAuthor().getName();
		this.quantity = cart.getQuantity();
		this.price = cart.getBook().getPrice();
		this.bookId = cart.getBook().getBookId();
	}
}
