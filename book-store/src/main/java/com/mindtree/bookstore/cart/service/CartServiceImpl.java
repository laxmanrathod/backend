package com.mindtree.bookstore.cart.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mindtree.bookstore.cart.dto.CartDto;
import com.mindtree.bookstore.cart.repository.CartRepository;
import com.mindtree.bookstore.entity.Book;
import com.mindtree.bookstore.entity.Cart;
import com.mindtree.bookstore.entity.User;

@Service
public class CartServiceImpl implements CartService {

	@Autowired
	private CartRepository cartRepository;

	@Transactional
	@Override
	public void addCartItem(User user, CartDto cartDto, Book book) {
		Cart cart = populateCart(user, cartDto, book);
		cartRepository.save(cart);
	}

	// deserializes the cart dto and returns Cart entity
	private Cart populateCart(User user, CartDto cartDto, Book book) {
		Cart cart = new Cart();
		cart.setQuantity(cartDto.getQuantity());
		cart.setBook(book);
		cart.setUser(user);
		cart.setActive(true);
		cart.setEffectiveDate(LocalDate.now());
		return cart;
	}

	@Override
	public List<Cart> getCartItems(User user) {
		return cartRepository.getAllActiveEffectiveCartItems(user, LocalDate.now());
	}
}
