package com.mindtree.bookstore.cart.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.bookstore.book.service.BookService;
import com.mindtree.bookstore.cart.dto.CartDto;
import com.mindtree.bookstore.cart.dto.CartItemResource;
import com.mindtree.bookstore.cart.service.CartService;
import com.mindtree.bookstore.entity.Book;
import com.mindtree.bookstore.entity.Cart;
import com.mindtree.bookstore.entity.User;
import com.mindtree.bookstore.enums.ApiErrorCode;
import com.mindtree.bookstore.exception.ApplicationException;
import com.mindtree.bookstore.user.service.UserService;

@RequestMapping("/cart")
@RestController
@CrossOrigin
public class CartController {

	@Autowired
	private CartService cartService;
	@Autowired
	private BookService bookService;
	@Autowired
	private UserService userService;

	@PostMapping("/addCartItem/{userId}")
	public ResponseEntity<?> addCartItem(@PathVariable int userId, @RequestBody CartDto dto) {
		User user = userService.getUser(userId);
		Book book = bookService.findBookById(dto.getBookId());

		if (dto.getQuantity() <= 0) {
			throw new ApplicationException(ApiErrorCode.NOT_ALLOWED);
		}
		if (user == null) {
			throw new ApplicationException(ApiErrorCode.DEFAULT_404, "User not found");
		}
		if (book == null) {
			throw new ApplicationException(ApiErrorCode.DEFAULT_404, "Book not found");
		}
		cartService.addCartItem(user, dto, book);
		return ResponseEntity.noContent().build();

	}

	@GetMapping("/items/{userId}")
	public List<CartItemResource> getCartItems(@PathVariable int userId) {
		User user = userService.getUser(userId);
		if (user == null)
			throw new ApplicationException(ApiErrorCode.DEFAULT_404, "User not found");
		List<Cart> carts = cartService.getCartItems(user);
		return carts.stream().map(c -> new CartItemResource(c)).collect(Collectors.toList());
	}

}
