package com.mindtree.bookstore.cart.dto;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CartDto {

	@NotNull
	private Integer quantity;

	@NotNull
	private Integer bookId;

	public CartDto(Integer quantity, Integer book) {
		super();
		this.quantity = quantity;
		this.bookId = book;
	}

}
