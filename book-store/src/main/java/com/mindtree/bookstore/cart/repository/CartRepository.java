package com.mindtree.bookstore.cart.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mindtree.bookstore.entity.Cart;
import com.mindtree.bookstore.entity.User;

@Repository
public interface CartRepository extends JpaRepository<Cart, Integer> {

	@Query("select c from Cart c where c.user = :user and c.isActive = True and c.expirationDate >= :referenceDate"
			+ " and c.effectiveDate <= :referenceDate")
	public List<Cart> getAllActiveEffectiveCartItems(@Param("user") User user,
			@Param("referenceDate") LocalDate referenceDate);

}
