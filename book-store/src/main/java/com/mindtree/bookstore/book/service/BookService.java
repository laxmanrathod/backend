package com.mindtree.bookstore.book.service;

import java.util.List;

import com.mindtree.bookstore.entity.Author;
import com.mindtree.bookstore.entity.Book;

public interface BookService {

	
	/**
	 * returns List of Books of a particular Author
	 * @param author
	 * @return List<Book>
	 */
	public List<Book> getBooksByAuthor(Author author);

	
	/**
	 * returns Book corresponding to the ID passed
	 * @param bookId
	 * @return Book
	 */
	public Book findBookById(Integer bookId);

}
