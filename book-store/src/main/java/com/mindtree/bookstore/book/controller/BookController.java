package com.mindtree.bookstore.book.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mindtree.bookstore.author.service.AuthorService;
import com.mindtree.bookstore.book.dto.BookDto;
import com.mindtree.bookstore.book.service.BookService;
import com.mindtree.bookstore.entity.Author;
import com.mindtree.bookstore.entity.Book;
import com.mindtree.bookstore.enums.ApiErrorCode;
import com.mindtree.bookstore.exception.ApplicationException;

@RestController
@CrossOrigin
@RequestMapping("/book")
public class BookController {

	@Autowired
	private BookService bookService;

	@Autowired
	private AuthorService authorService;

	@GetMapping("/author/{authorId}")
	public List<BookDto> getBooksByAuthor(@PathVariable int authorId) {
		Author author = authorService.getAuthor(authorId);
		if(author == null) {
			throw new ApplicationException(ApiErrorCode.DEFAULT_404, "Author not found");
		}
		List<Book> bookList = bookService.getBooksByAuthor(author);
		return bookList.stream().map(book -> new BookDto(book)).collect(Collectors.toList());
	}
	
	@GetMapping("/{bookId}")
	public BookDto getBook(@PathVariable int bookId) {
		Book book = bookService.findBookById(bookId);
		if(book == null) {
			throw new ApplicationException(ApiErrorCode.DEFAULT_404, "Book not found");
		}
		return new BookDto(book);
	}

}
